﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flower : MonoBehaviour
{
    public FlowerSpawner homeSpawner;

    public List<Bee> collectingBees;

    public int flowerMaxCapacity;
    public int flowerCurrentCapacity;
      
    public bool isCollectable;
    private bool isRecovering;

    public List<GameObject> FlowersAccessor;

    void Start()
    {
        flowerMaxCapacity = (int)TransferData.flowersMaxCapacity;
        flowerCurrentCapacity = flowerMaxCapacity;
        isRecovering = false;
        isCollectable = true;
    }

    void Update()
    {
        if (collectingBees.Count == 5)
        {
            isCollectable = false;
        }
        else
        {
            isCollectable = true;
        }
        if (flowerCurrentCapacity < flowerMaxCapacity && isRecovering == false)
        {
            isRecovering = true;
            StartCoroutine(RecoverTime());
            flowerCurrentCapacity += 1;            
        }
        if (flowerCurrentCapacity == 0)
        {
            homeSpawner.isFlower = false;
            Destroy(gameObject);
        }
    }

    private IEnumerator RecoverTime()
    {
        yield return new WaitForSeconds(1);
        isRecovering = false;
    }
}