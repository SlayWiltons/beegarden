﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlowerSpawner : MonoBehaviour
{
    public GameObject[] flowers;
    private GameObject newFlower;
    
    public int timeToGrow;

    public bool isFlower = false;

    private void Awake()
    {
        StartCoroutine(GrowingProcess(0));
    }

    private void Update()
    {
        if (isFlower == false)
        {
            StartCoroutine(GrowingProcess(timeToGrow));            
        }        
    }

    private IEnumerator GrowingProcess(int _time)
    {
        isFlower = true;
        yield return new WaitForSeconds(_time);
        Vector3 flowerPosition = new Vector3(transform.position.x, transform.position.y, transform.position.z);
        newFlower = Instantiate(flowers[Random.Range(0, flowers.Length)], flowerPosition, Quaternion.identity);
        newFlower.transform.parent = transform;
        newFlower.GetComponent<Flower>().homeSpawner = this;        
    }
}