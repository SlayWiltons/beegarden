﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ParamsMenu : MonoBehaviour
{
    [SerializeField]private Slider sliderInsInHive;
    [SerializeField] private Slider sliderInsCreate;
    [SerializeField] private Slider sliderInsLifeTime;
    [SerializeField] private Slider sliderHiveCapacity;
    [SerializeField] private Slider sliderFlowerCapacity;

    [SerializeField] private InputField inputInsInHive;
    [SerializeField] private InputField inputInsCreate;
    [SerializeField] private InputField inputInsLifeTime;
    [SerializeField] private InputField inputHiveCapacity;
    [SerializeField] private InputField inputFlowerCapacity;

    [SerializeField] private GameObject MainMenuPanel;
    [SerializeField] private GameObject ParamsMenuPanel;

    private void Start()
    {
        //Reset();
    }

    public void SliderInsInHiveChange()
    {
        ChangeSliderValue(inputInsInHive, sliderInsInHive);
    }

    public void SliderInsCreateChange()
    {
        ChangeSliderValue(inputInsCreate, sliderInsCreate);
    }

    public void SliderInsLifetimeChange()
    {
        ChangeSliderValue(inputInsLifeTime, sliderInsLifeTime);
    }

    public void SliderHiveCapacityChange()
    {
        ChangeSliderValue(inputHiveCapacity, sliderHiveCapacity);
    }

    public void SliderFlowerCapacityChange()
    {
        ChangeSliderValue(inputFlowerCapacity, sliderFlowerCapacity);
    }

    public void InputInsInHiveChange()
    {
        ChangeInputValue(sliderInsInHive, inputInsInHive);
    }

    public void InputInsCreateChange()
    {
        ChangeInputValue(sliderInsCreate, inputInsCreate);
    }

    public void InputInsLifetimeChange()
    {
        ChangeInputValue(sliderInsLifeTime, inputInsLifeTime);
    }

    public void InputHiveCapacityChange()
    {
        ChangeInputValue(sliderHiveCapacity, inputHiveCapacity);
    }

    public void InputFlowerCapacityChange()
    {
        ChangeInputValue(sliderFlowerCapacity, inputFlowerCapacity);
    }

    public void StartNewGame()
    {
        TransferData.creaturesInHive = sliderInsInHive.value;
        TransferData.timeToBorn = sliderInsCreate.value;
        TransferData.lifeTime = sliderInsLifeTime.value;
        TransferData.hiveMaxCapacity = sliderHiveCapacity.value;
        TransferData.flowersMaxCapacity = sliderFlowerCapacity.value;
        SceneManager.LoadScene(1);
    }

    public void Back()
    {
        Reset();
        ParamsMenuPanel.SetActive(false);
        MainMenuPanel.SetActive(true);
    }

    private void Reset()
    {
        sliderInsInHive.value = sliderInsInHive.minValue;
        inputInsInHive.text = sliderInsInHive.value.ToString();
        sliderInsInHive.interactable = true;
        inputInsInHive.interactable = true;

        sliderInsCreate.value = sliderInsCreate.minValue;
        inputInsCreate.text = sliderInsInHive.value.ToString();
        sliderInsCreate.interactable = true;
        inputInsCreate.interactable = true;

        sliderInsLifeTime.value = sliderInsLifeTime.minValue;
        inputInsLifeTime.text = sliderInsLifeTime.value.ToString();
        sliderInsLifeTime.interactable = true;
        inputInsLifeTime.interactable = true;

        sliderHiveCapacity.value = sliderHiveCapacity.minValue;
        inputHiveCapacity.text = sliderHiveCapacity.value.ToString();
        sliderHiveCapacity.interactable = true;
        inputHiveCapacity.interactable = true;

        sliderFlowerCapacity.value = sliderFlowerCapacity.minValue;
        inputFlowerCapacity.text = sliderFlowerCapacity.value.ToString();
        sliderFlowerCapacity.interactable = true;
        inputFlowerCapacity.interactable = true;
    }

    private void ChangeInputValue(Slider _slider, InputField _input)
    {
        _input.text = _slider.value.ToString();
    }

    private void ChangeSliderValue(InputField _input, Slider _slider)
    {
        if (float.TryParse(_input.text, out float floatTest) == false)
        {
            _input.text = _slider.minValue.ToString();
            _slider.value = _slider.minValue;
        }
        if (float.Parse(_input.text) < _slider.minValue)
        {
            _input.text = _slider.minValue.ToString();
            _slider.value = _slider.minValue;
        }
        if (float.Parse(_input.text) > _slider.maxValue)
        {
            _input.text = _slider.maxValue.ToString();
            _slider.value = _slider.maxValue;
        }
        _slider.value = float.Parse(_input.text);
    }
}