﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowInfo : MonoBehaviour
{
    public GameObject infoPanel;
    public GameObject chosenHive;
    public Text beesText;
    public Text dronesText;
    public Text honeyText;
    public Text moneyText;
    public Button sellButton;
    public int currentHoney;
    public int maxHoney;
    public int money;

    private void Start()
    {
        moneyText.text = "0";
        money = 0;
        infoPanel.SetActive(false);
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        { 
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform.gameObject.CompareTag("Hive"))
                {
                    chosenHive = hit.transform.gameObject;
                    infoPanel.SetActive(true);                    
                }
            }
        }
        Stats();
    }

    private void Stats()
    {
        if (infoPanel.activeSelf == true)
        {
            beesText.text = chosenHive.GetComponent<HiveController>().bees.Count.ToString();
            dronesText.text = chosenHive.GetComponent<HiveController>().drones.Count.ToString();
            currentHoney = chosenHive.GetComponent<HiveController>().currentCapacity;
            maxHoney = chosenHive.GetComponent<HiveController>().maxCapacity;
            honeyText.text = currentHoney.ToString() + "/" + maxHoney.ToString();
            if (currentHoney / 10 > 0)
            {
                sellButton.interactable = true;
            }
            else
            {
                sellButton.interactable = false;
            }
            if (currentHoney == maxHoney)
            {
                honeyText.color = Color.red;
            }
            else
            {
                honeyText.color = Color.black;
            }
        }
    }

    public void CloseStatsPanel()
    {
        infoPanel.SetActive(false);
        beesText.text = "0";
        dronesText.text = "0";
        currentHoney = 0;
        maxHoney = 0;
        honeyText.text = "0/0";
        sellButton.interactable = false;
    }

    public void SellHoney()
    {
        money = money + (currentHoney / 10);        
        moneyText.text = money.ToString();
        chosenHive.GetComponent<HiveController>().currentCapacity = chosenHive.GetComponent<HiveController>().currentCapacity - (currentHoney / 10) * 10;
        sellButton.interactable = false;
    }
}