﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private Slider sliderInsInHive;
    [SerializeField] private Slider sliderInsCreate;
    [SerializeField] private Slider sliderInsLifeTime;
    [SerializeField] private Slider sliderHiveCapacity;
    [SerializeField] private Slider sliderFlowerCapacity;

    [SerializeField] private InputField inputInsInHive;
    [SerializeField] private InputField inputInsCreate;
    [SerializeField] private InputField inputInsLifeTime;
    [SerializeField] private InputField inputHiveCapacity;
    [SerializeField] private InputField inputFlowerCapacity;

    [SerializeField] private GameObject MainMenuPanel;
    [SerializeField] private GameObject ParamsMenuPanel;

    public void NewGame()
    {
        MainMenuPanel.SetActive(false);
        ParamsMenuPanel.SetActive(true);
    }

    public void DemoGame()
    {
        inputInsInHive.text = "100";
        sliderInsInHive.value = float.Parse(inputInsInHive.text);
        inputInsInHive.interactable = false;
        sliderInsInHive.interactable = false;

        inputInsCreate.text = "3";
        sliderInsCreate.value = float.Parse(inputInsCreate.text);
        inputInsCreate.interactable = false;
        sliderInsCreate.interactable = false;

        inputInsLifeTime.text = "30";
        sliderInsLifeTime.value = float.Parse(inputInsLifeTime.text);
        inputInsLifeTime.interactable = false;
        sliderInsLifeTime.interactable = false;

        inputHiveCapacity.text = "2000";
        sliderHiveCapacity.value = float.Parse(inputHiveCapacity.text);
        inputHiveCapacity.interactable = false;
        sliderHiveCapacity.interactable = false;

        inputFlowerCapacity.text = "200";
        sliderFlowerCapacity.value = float.Parse(inputFlowerCapacity.text);
        inputFlowerCapacity.interactable = false;
        sliderFlowerCapacity.interactable = false;

        MainMenuPanel.SetActive(false);
        ParamsMenuPanel.SetActive(true);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
