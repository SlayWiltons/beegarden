﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InGameMenu : MonoBehaviour
{
    [SerializeField] private GameObject menuPanel;

    private void Start()
    {
        menuPanel.SetActive(false);
    }

    public void ShowInGameMenu()
    {
        Time.timeScale = 0;
        menuPanel.SetActive(true);
    }

    public void ResumeTheGame()
    {
        menuPanel.SetActive(false);
        Time.timeScale = 1;
    }

    public void RestartTheGame()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(1);
    }

    public void ReturnToMainMenu()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(0);
    }

    public void Quit()
    {
        Application.Quit();
    }
}