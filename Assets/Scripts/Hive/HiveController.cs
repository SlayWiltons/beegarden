﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class HiveController : MonoBehaviour
{
    [SerializeField]private GameObject beePrefab;
    [SerializeField]private GameObject dronePrefab;
    private GameObject newChar;


    public List<GameObject> characters;
    public List<GameObject> bees;
    public List<GameObject> drones;    
    public List<GameObject> availableFlowers;
    private List<GameObject> allFlowers;

    public Slider visualCapacity;

    public int beesQuantity;
    public int currentCapacity;
    public int maxCapacity;
    public int timeToBorn; 

    private bool isCreating;
    private bool isHiveFull;

    void Start()
    {
        beesQuantity = (int)TransferData.creaturesInHive;
        timeToBorn = (int)TransferData.timeToBorn;
        maxCapacity = (int)TransferData.hiveMaxCapacity;
        characters = new List<GameObject>();
        bees = new List<GameObject>();
        drones = new List<GameObject>();
        allFlowers = new List<GameObject>();
        availableFlowers = new List<GameObject>();
        visualCapacity.maxValue = maxCapacity;
        isCreating = false;
    }

    private void Update()
    {
        if (characters.Count < beesQuantity && isCreating == false)
        StartCoroutine(beesCreator(timeToBorn));
        if (characters.Count == beesQuantity && isHiveFull == false)
        {
            isHiveFull = true;
            if (drones.Count != 0)
            {
                Decimation(drones);
            }
            else
            {
                Decimation(bees);
            }
        }
        visualCapacity.value = currentCapacity;
    }    

    private void CreateCharacter(GameObject _prefab, List<GameObject> _charactersType)
    {
        Vector3 charPosition = new Vector3 (gameObject.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z);
        newChar = Instantiate(_prefab, charPosition, Quaternion.identity);
        newChar.transform.parent = transform;
        newChar.GetComponent<AbstractCharacter>().homePoint = transform;
        newChar.GetComponent<AbstractCharacter>().homeHive = gameObject;
        characters.Add(newChar);
        _charactersType.Add(newChar);
    }

    public void ChoosingFlower()
    {        
        availableFlowers.Clear();
        allFlowers = GameObject.FindGameObjectsWithTag("Flower").ToList();
        for (int i = 0; i < allFlowers.Count; i++)
        {
            if (allFlowers[i].GetComponent<Flower>().isCollectable == true)
            {
                availableFlowers.Add(allFlowers[i]);
            }
        }
        allFlowers.Clear();
    }

    public void Decimation(List<GameObject> _listOfCharacters)
    {
        StartCoroutine(TimeToDie());
        if (_listOfCharacters.Count >= 1)
        {
            int randId = Random.Range(0, _listOfCharacters.Count - 1);
            GameObject outsider = _listOfCharacters[randId];
            _listOfCharacters.Remove(outsider);
            characters.Remove(outsider);
            Destroy(outsider);
        }
        else
        {
            GameObject outsider = _listOfCharacters[0];
            characters.Remove(outsider);
            _listOfCharacters.Clear();
            _listOfCharacters = null;
        }       
    }

    public IEnumerator beesCreator(int _time)
    {
        isCreating = true;
        yield return new WaitForSeconds(_time);
        float chance = Random.value;
        if (chance >= 0.3)
        {
            CreateCharacter(beePrefab, bees);
        }
        else if (chance < 0.3)
        {
            CreateCharacter(dronePrefab, drones);
        }
        isCreating = false;
    }

    private IEnumerator TimeToDie()
    {
        yield return new WaitForSeconds(3);
        isHiveFull = false;
    }
}