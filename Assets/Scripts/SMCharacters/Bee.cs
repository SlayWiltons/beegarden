﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Bee : AbstractCharacter
{
    public ICharState currentState;

    public GameObject chosenFlower;

    [SerializeField] GameObject model;

    public Transform flowerPoint;

    public int maxCapacity;
    public int capacity;    
    public int speed;  
    
    public DoNothingState doNothingState = new DoNothingState();
    public MoveToFlowerState moveToFlowerState = new MoveToFlowerState();
    public ReturnToHiveState returnToHiveState = new ReturnToHiveState();
    public CollectingHoneyState collectingHoneyState = new CollectingHoneyState();
    public FillingTheHiveState fillingTheHiveState = new FillingTheHiveState();

    private void Start()
    {
        currentState = doNothingState;
        StartCoroutine(CharacterDie(homeHive.GetComponent<HiveController>().bees, gameObject));
    }

    private void Update()
    {
        currentState = currentState.SetState(this);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Hive"))
        {
            model.SetActive(false);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Hive"))
        {
            model.SetActive(true);
        }
    }
}