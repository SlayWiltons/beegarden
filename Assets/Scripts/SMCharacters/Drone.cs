﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Drone : AbstractCharacter
{
    private void Start()
    {
        currentStateName = "В улье";
        StartCoroutine(CharacterDie(homeHive.GetComponent<HiveController>().drones, gameObject));
    }
}