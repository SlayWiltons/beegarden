﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AbstractCharacter : MonoBehaviour
{   
    public GameObject homeHive;

    public Transform homePoint;

    public float destrTime;

    public string currentStateName;

    public virtual IEnumerator CharacterDie(List<GameObject>_currentList, GameObject _gameobject)
    {
        yield return new WaitForSeconds(destrTime = TransferData.lifeTime);
        homeHive.GetComponent<HiveController>().characters.Remove(_gameobject);
        _currentList.Remove(_gameobject);
        Destroy(_gameobject);
    }
}