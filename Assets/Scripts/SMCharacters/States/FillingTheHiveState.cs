﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FillingTheHiveState : ICharState
{
    private bool isFilling = false;
    private int deltaCapacity;

    public ICharState SetState(Bee character)
    {
        character.currentStateName = "Сдает мёд";
        if (character.capacity > 0 && character.homeHive.GetComponent<HiveController>().currentCapacity < character.homeHive.GetComponent<HiveController>().maxCapacity)
        {
            FillingHive(character);
            return character.fillingTheHiveState;
        }
        else
        {
            return character.doNothingState;
        }
    }

    private void FillingHive(Bee character)
    {
        if (isFilling == false)
        {
            isFilling = true;           
            if (character.capacity < 10)
            {
                character.homeHive.GetComponent<HiveController>().currentCapacity += character.capacity;
                character.capacity = 0;
            }
            else
            {
                character.capacity -= 10;
                character.homeHive.GetComponent<HiveController>().currentCapacity += 10;             
                character.StartCoroutine(DelayFillingHive());
            }
        }
    }

    private IEnumerator DelayFillingHive()
    {
        yield return new WaitForSeconds(2);
        isFilling = false;
    }
}