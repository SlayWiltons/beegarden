﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class DoNothingState : ICharState
{
    private int deltaCapacity;

    public ICharState SetState(Bee character)
    {
        StayInHive(character);
        character.currentStateName = "В улье";
        if (character.flowerPoint != null && character.homeHive.GetComponent<HiveController>().currentCapacity < character.homeHive.GetComponent<HiveController>().maxCapacity)
        {            
            if (character.capacity > 0 )
            {
                
                return character.fillingTheHiveState;
            }
            else
            {
                return character.moveToFlowerState;
            }
        }
        else
        {
            if (character.homeHive.GetComponent<HiveController>().currentCapacity > character.homeHive.GetComponent<HiveController>().maxCapacity)
            {
                deltaCapacity = character.homeHive.GetComponent<HiveController>().currentCapacity - character.homeHive.GetComponent<HiveController>().maxCapacity;
                character.homeHive.GetComponent<HiveController>().currentCapacity -= deltaCapacity;
                character.capacity += deltaCapacity;
            }
            
            ChooseFlower(character);
            return character.doNothingState;
        }
    }

    private void StayInHive(Bee character)
    {
        character.transform.position = character.homePoint.position;
    }

    private void ChooseFlower(Bee character)
    {
        character.homeHive.GetComponent<HiveController>().ChoosingFlower();
        if (character.homeHive.GetComponent<HiveController>().availableFlowers != null || character.homeHive.GetComponent<HiveController>().availableFlowers.Count != 0)
        {
            int randId = Random.Range(0, character.homeHive.GetComponent<HiveController>().availableFlowers.Count - 1);
            character.chosenFlower = character.homeHive.GetComponent<HiveController>().availableFlowers[randId];
            Transform capturePoint = character.chosenFlower.transform.Find("CapturePoint");
            character.flowerPoint = capturePoint;
            character.chosenFlower.GetComponent<Flower>().collectingBees.Add(character);
        }
    }
}