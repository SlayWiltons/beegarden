﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectingHoneyState : ICharState
{
    private bool isCollecting = false;
    public ICharState SetState(Bee character)
    {
        character.currentStateName = "Собирает мёд";
        if (character.flowerPoint != null && character.capacity < character.maxCapacity)
        {
            CollectingHoney(character);
            return character.collectingHoneyState;
        }
        else
        {
            return character.returnToHiveState;
        }
    }

    private void CollectingHoney(Bee character)
    {
        if (isCollecting == false)
        {
            isCollecting = true;
            if (character.chosenFlower.GetComponent<Flower>().flowerCurrentCapacity < 10)
            {
                character.capacity += character.chosenFlower.GetComponent<Flower>().flowerCurrentCapacity;
                character.chosenFlower.GetComponent<Flower>().flowerCurrentCapacity = 0;
            }
            else
            {
                character.capacity += 10;
                character.chosenFlower.GetComponent<Flower>().flowerCurrentCapacity -= 10;
            }
            character.StartCoroutine(DelayCollectingHoney());
        }
    }

    private IEnumerator DelayCollectingHoney()
    {        
        yield return new WaitForSeconds(2);
        isCollecting = false;
    }
}