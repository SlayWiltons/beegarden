﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReturnToHiveState : ICharState
{
    public ICharState SetState(Bee character)
    {
        character.currentStateName = "Возвращается в улей";
        ReturningToHive(character);
        if (character.flowerPoint == null || character.capacity == character.maxCapacity)
        {
            if (character.transform.position != character.homePoint.position)
            {
                return character.returnToHiveState;
            }
            else
            {
                return character.fillingTheHiveState;
            }
        }
        else
        {
            return character.currentState;
        }
    }

    private void ReturningToHive(Bee character)
    {
        character.transform.position = Vector3.MoveTowards(character.transform.position, character.homePoint.position, Time.deltaTime * character.speed);
        character.transform.rotation = Quaternion.LookRotation(character.homePoint.position - character.transform.position);
    }
}