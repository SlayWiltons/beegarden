﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveToFlowerState : ICharState
{
    public ICharState SetState(Bee character)
    {
        character.currentStateName = "Летит к цветку";
        if (character.flowerPoint != null)
        {
            MovingToFlower(character);
            if (character.transform.position != character.flowerPoint.position)
            {
                return character.moveToFlowerState;
            }
            else
            {
                return character.collectingHoneyState;
            }
        }
        else
        {
            return character.returnToHiveState;
        }
    }

    private void MovingToFlower(Bee character)
    {
        character.transform.position = Vector3.MoveTowards(character.transform.position, character.flowerPoint.position, Time.deltaTime * character.speed);
        character.transform.rotation = Quaternion.LookRotation(character.flowerPoint.position - character.transform.position);
    }
}