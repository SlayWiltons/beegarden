﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransferData : MonoBehaviour
{
    public static float creaturesInHive;
    public static float timeToBorn;
    public static float lifeTime;
    public static float hiveMaxCapacity;
    public static float flowersMaxCapacity;
}
